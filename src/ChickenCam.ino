#include "esp_camera.h"
#include <WiFi.h>
#include <WiFiClient.h>
#include <HTTPClient.h>
#include <Ticker.h>
#include "esp_system.h"

const int wdtTimeout = 10000;  //time in ms to trigger the watchdog
hw_timer_t *timer = NULL;

#define CAMERA_MODEL_AI_THINKER

#include "camera_pins.h"

Ticker lightTimer;

//const char* ssid = "FRITZ!Box 7430 DY";
//const char* password = "testcase7$OK";

const char* ssid = "ZyXEL WAP3205 v3";
const char* password = "FRGHK44447";

//const char* ssid = "R-Net 2,4GHz";
//const char* password = "KommtZeitKommtRat";

void IRAM_ATTR resetModule() {
  ets_printf("reboot\n");
  esp_restart();
}

void setup() {
  Serial.begin(115200);
  Serial.setDebugOutput(true);
  Serial.println();

  pinMode(2, OUTPUT);
  lightOff();

  camera_config_t config;
  config.ledc_channel = LEDC_CHANNEL_0;
  config.ledc_timer = LEDC_TIMER_0;
  config.pin_d0 = Y2_GPIO_NUM;
  config.pin_d1 = Y3_GPIO_NUM;
  config.pin_d2 = Y4_GPIO_NUM;
  config.pin_d3 = Y5_GPIO_NUM;
  config.pin_d4 = Y6_GPIO_NUM;
  config.pin_d5 = Y7_GPIO_NUM;
  config.pin_d6 = Y8_GPIO_NUM;
  config.pin_d7 = Y9_GPIO_NUM;
  config.pin_xclk = XCLK_GPIO_NUM;
  config.pin_pclk = PCLK_GPIO_NUM;
  config.pin_vsync = VSYNC_GPIO_NUM;
  config.pin_href = HREF_GPIO_NUM;
  config.pin_sscb_sda = SIOD_GPIO_NUM;
  config.pin_sscb_scl = SIOC_GPIO_NUM;
  config.pin_pwdn = PWDN_GPIO_NUM;
  config.pin_reset = RESET_GPIO_NUM;
  config.xclk_freq_hz = 20000000;
  config.pixel_format = PIXFORMAT_JPEG;
  if(psramFound())
  {
    config.frame_size = FRAMESIZE_UXGA;
    config.jpeg_quality = 10;
    config.fb_count = 2;
  } 
  else 
  {
    config.frame_size = FRAMESIZE_SVGA;
    config.jpeg_quality = 12;
    config.fb_count = 1;
  }

  timer = timerBegin(0, 80, true);                  //timer 0, div 80
  timerAttachInterrupt(timer, &resetModule, true);  //attach callback
  timerAlarmWrite(timer, wdtTimeout * 1000, false); //set time in us
  timerAlarmEnable(timer);                          //enable interrupt

  // camera init
  esp_err_t err = esp_camera_init(&config);
  if (err != ESP_OK) {
    Serial.printf("Camera init failed with error 0x%x", err);
    ESP.restart();
  }

  sensor_t * s = esp_camera_sensor_get();

  if (s->id.PID == OV3660_PID) 
  {
    s->set_vflip(s, 1);//flip it back
    s->set_brightness(s, 1);//up the blightness just a bit
    s->set_saturation(s, -2);//lower the saturation
  }
  s->set_framesize(s, FRAMESIZE_CIF);

  WiFi.begin(ssid, password);
  WiFi.printDiag(Serial);

  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }
  Serial.println("");
  Serial.println("WiFi connected");

  Serial.print("Got IP: ");
  Serial.print(WiFi.localIP());
  Serial.println();
}

void lightOff()
{
  Serial.println("Licht aus !!!");
  digitalWrite(2, LOW);
}

void loop() {
  delay(500);

  camera_fb_t * fb = NULL;
  fb = esp_camera_fb_get();

  if(fb->format == PIXFORMAT_JPEG){

    WiFiClient client;
    
    HTTPClient getClient;
    getClient.begin(client, "http://kerstin.roehrach.de/lichtAn.php");
    getClient.GET();
    if (getClient.getString().startsWith("1"))
    {
      Serial.println("Licht an !!!");
      digitalWrite(2, HIGH);
      lightTimer.once(10, lightOff);
    }
    getClient.end();

    if (client.connect("kerstin.roehrach.de", 80)) {
        Serial.println("Connected ok!");
        size_t len = fb->len;
        client.println("POST /upload.php HTTP/1.1");
        client.println("Host: kerstin.roehrach.de"); 
        client.println("Connection: close");
        client.println("Cache-Control: max-age=0");
        client.println("Content-Type: multipart/form-data; boundary=----WebKitFormBoundaryjg2qVIUS8teOAbN3");
        client.println("Content-Length: " + String(len+202)); 
        client.println();
        client.println("------WebKitFormBoundaryjg2qVIUS8teOAbN3");
        client.println("Content-Disposition: form-data; name=\"imageFile\"; filename=\"chicken.jpg\""); 
        client.println("Content-Type: application/octet-stream");
        client.println();
        Serial.println("SIZE => " + String(len));
        client.write(fb->buf, len);
        client.println(); //file end
        client.println("------WebKitFormBoundaryjg2qVIUS8teOAbN3--");
        client.println();
  
        Serial.println("Got JPEG");
        
        if (client.available()) 
        {
          Serial.println("Data available");
          String serverRes = client.readStringUntil('}');
          Serial.println(serverRes);
        }

        timerWrite(timer, 0); //reset timer (feed watchdog)
    }
  } 
  esp_camera_fb_return(fb);
} 
